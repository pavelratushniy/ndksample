//
// Created by Pavel Ratushnyi on 05.03.2020.
//

#ifndef NDKSAMPLE_STORE_H
#define NDKSAMPLE_STORE_H

#include <jni.h>
#include <string.h>

#define STORE_MAX_CAPACITY 16

typedef enum {
    STORETYPE_STRING,
    STORETYPE_INTEGER,
    STORETYPE_COLOR,
    STORETYPE_STRING_ARRAY,
    STORETYPE_INTEGER_ARRAY,
    STORETYPE_COLOR_ARRAY
} StoreType;

typedef union {
    char *string;
    int32_t integer;
    jobject color;
    char **stringArray;
    int32_t *integerArray;
    jobject *colorArray;
} StoreValue;

typedef struct {
    char *key;
    StoreType type;
    StoreValue value;
    int32_t length;
} StoreEntry;

typedef struct {
    StoreEntry entries[STORE_MAX_CAPACITY];
    int32_t length;
} Store;

bool isEntryValid(JNIEnv *env, StoreEntry *entry, StoreType type);

StoreEntry *allocateEntry(JNIEnv *env, Store *store, jstring key);

StoreEntry *findEntry(JNIEnv *env, Store *store, jstring key);

void releaseEntryValue(JNIEnv *env, StoreEntry *entry);

Store *getStore();

void throwInvalidTypeException(JNIEnv *env);

void throwNotExistingException(JNIEnv *env);

void throwStoreFullException(JNIEnv *env);

#endif //NDKSAMPLE_STORE_H
