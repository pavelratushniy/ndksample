//
// Created by Pavel Ratushnyi on 28.02.2020.
//

#ifndef NDKSAMPLE_COM_CLOUDMADE_NDKSAMPLE_STORE_STORE_H
#define NDKSAMPLE_COM_CLOUDMADE_NDKSAMPLE_STORE_STORE_H

#include <jni.h>
#include <cstdlib>

extern "C"
JNIEXPORT jint JNICALL
Java_com_cloudmade_ndksample_store_Store_getCount(JNIEnv *env, jobject thiz);

extern "C"
JNIEXPORT jstring JNICALL
Java_com_cloudmade_ndksample_store_Store_getString(JNIEnv *env, jobject thiz, jstring key);

extern "C"
JNIEXPORT void JNICALL
Java_com_cloudmade_ndksample_store_Store_setString(JNIEnv *env, jobject thiz, jstring key,
                                                   jstring value);

extern "C"
JNIEXPORT jint JNICALL
Java_com_cloudmade_ndksample_store_Store_getInteger(JNIEnv *env, jobject thiz, jstring key);

extern "C"
JNIEXPORT void JNICALL
Java_com_cloudmade_ndksample_store_Store_setInteger(JNIEnv *env, jobject thiz, jstring key,
                                                    jint value);

extern "C"
JNIEXPORT jobject JNICALL
Java_com_cloudmade_ndksample_store_Store_getColor(JNIEnv *env, jobject thiz, jstring key);

extern "C"
JNIEXPORT void JNICALL
Java_com_cloudmade_ndksample_store_Store_setColor(JNIEnv *env, jobject thiz, jstring key,
                                                  jobject value);

extern "C"
JNIEXPORT jobjectArray JNICALL
Java_com_cloudmade_ndksample_store_Store_getStringArray(JNIEnv *env, jobject thiz, jstring key);

extern "C"
JNIEXPORT void JNICALL
Java_com_cloudmade_ndksample_store_Store_setStringArray(JNIEnv *env, jobject thiz, jstring key,
                                                        jobjectArray value);

extern "C"
JNIEXPORT jintArray JNICALL
Java_com_cloudmade_ndksample_store_Store_getIntegerArray(JNIEnv *env, jobject thiz, jstring key);

extern "C"
JNIEXPORT void JNICALL
Java_com_cloudmade_ndksample_store_Store_setIntegerArray(JNIEnv *env, jobject thiz, jstring key,
                                                         jintArray value);

extern "C"
JNIEXPORT jobjectArray JNICALL
Java_com_cloudmade_ndksample_store_Store_getColorArray(JNIEnv *env, jobject thiz, jstring key);

extern "C"
JNIEXPORT void JNICALL
Java_com_cloudmade_ndksample_store_Store_setColorArray(JNIEnv *env, jobject thiz, jstring key,
                                                       jobjectArray value);

#endif //NDKSAMPLE_COM_CLOUDMADE_NDKSAMPLE_STORE_STORE_H
