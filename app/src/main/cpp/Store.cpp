//
// Created by Pavel Ratushnyi on 05.03.2020.
//

#include "Store.h"

namespace {
    Store store;
}

void releaseEntryValue(JNIEnv *env, StoreEntry *entry) {
    switch (entry->type) {
        case STORETYPE_STRING:
            delete entry->value.string;
            break;
        case STORETYPE_COLOR:
            env->DeleteGlobalRef(entry->value.color);
            break;
        case STORETYPE_STRING_ARRAY:
            for (int32_t i = 0; i < entry->length; i++) {
                delete entry->value.stringArray[i];
            }
            delete[] entry->value.stringArray;
            break;
        case STORETYPE_INTEGER_ARRAY:
            delete[] entry->value.integerArray;
            break;
        case STORETYPE_COLOR_ARRAY:
            for (int32_t i = 0; i < entry->length; i++) {
                env->DeleteGlobalRef(entry->value.colorArray[i]);
            }
            delete[] entry->value.colorArray;
            break;
    }
}

bool isEntryValid(JNIEnv *env, StoreEntry *entry, StoreType type) {
    if (entry ==  NULL) {
        throwNotExistingException(env);
    } else if (entry->type != type) {
        throwInvalidTypeException(env);
    }
    return !env->ExceptionCheck();
}

StoreEntry *allocateEntry(JNIEnv *env, Store *store, jstring key) {
    StoreEntry *entry = findEntry(env, store, key);
    if (entry != NULL) {
        releaseEntryValue(env, entry);
    } else {
        if (store->length >= STORE_MAX_CAPACITY) {
            throwStoreFullException(env);
            return NULL;
        }
        entry = store->entries + store->length;

        const char *tmpKey = env->GetStringUTFChars(key, NULL);
        entry->key = new char[strlen(tmpKey) + 1];
        strcpy(entry->key, tmpKey);
        env->ReleaseStringUTFChars(key, tmpKey);

        ++store->length;
    }
    return entry;
}

StoreEntry *findEntry(JNIEnv *env, Store *store, jstring key) {
    StoreEntry *entry = store->entries;
    StoreEntry *entryEnd = entry + store->length;

    const char *tmpKey = env->GetStringUTFChars(key, NULL);
    while ((entry < entryEnd) && (strcmp(entry->key, tmpKey) != 0)) {
        entry++;
    }
    env->ReleaseStringUTFChars(key, tmpKey);

    return entry == entryEnd ? NULL : entry;
}

Store *getStore() {
    return &store;
}

void throwInvalidTypeException(JNIEnv *env) {
    jclass clazz = env->FindClass("com/cloudmade/ndksample/store/InvalidTypeException");
    if (clazz != NULL) {
        env->ThrowNew(clazz, "Invalid type");
    }
    env->DeleteLocalRef(clazz);
}

void throwNotExistingException(JNIEnv *env) {
    jclass clazz = env->FindClass("com/cloudmade/ndksample/store/NotExistingKeyException");
    if (clazz != NULL) {
        env->ThrowNew(clazz, "Key does not exist");
    }
    env->DeleteLocalRef(clazz);
}

void throwStoreFullException(JNIEnv *env) {
    jclass clazz = env->FindClass("com/cloudmade/ndksample/store/StoreFullException");
    if (clazz != NULL) {
        env->ThrowNew(clazz, "Store is full");
    }
    env->DeleteLocalRef(clazz);
}
