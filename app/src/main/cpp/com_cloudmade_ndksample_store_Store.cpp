//
// Created by Pavel Ratushnyi on 28.02.2020.
//
#include "com_cloudmade_ndksample_store_Store.h"
#include "Store.h"

namespace {
    jclass stringClass;
    jclass colorClass;

    jmethodID onSuccessIntMethod;
    jmethodID onSuccessStringMethod;
    jmethodID onSuccessColorMethod;
}

JNIEXPORT jint JNI_OnLoad(JavaVM *pVM, void *reserved) {
    JNIEnv *env;
    if (pVM->GetEnv(reinterpret_cast<void **>(&env), JNI_VERSION_1_6) != JNI_OK) {
        abort();
    }
    jclass stringClassTmp = env->FindClass("java/lang/String");
    if (stringClassTmp == NULL) {
        abort();
    }
    stringClass = static_cast<jclass>(env->NewGlobalRef(stringClassTmp));
    env->DeleteLocalRef(stringClassTmp);

    jclass colorClassTmp = env->FindClass("com/cloudmade/ndksample/store/Color");
    if (colorClassTmp == NULL) {
        abort();
    }
    colorClass = static_cast<jclass>(env->NewGlobalRef(colorClassTmp));
    env->DeleteLocalRef(colorClassTmp);

    jclass StoreClass = env->FindClass("com/cloudmade/ndksample/store/Store");
    if (StoreClass == NULL) {
        abort();
    }

    onSuccessIntMethod = env->GetMethodID(StoreClass, "onSuccess", "(I)V");
    if (onSuccessIntMethod == NULL) {
        abort();
    }

    onSuccessStringMethod = env->GetMethodID(StoreClass, "onSuccess", "(Ljava/lang/String;)V");
    if (onSuccessStringMethod == NULL) {
        abort();
    }

    onSuccessColorMethod = env->GetMethodID(StoreClass, "onSuccess",
                                            "(Lcom/cloudmade/ndksample/store/Color;)V");
    if (onSuccessColorMethod == NULL) {
        abort();
    }

    env->DeleteLocalRef(StoreClass);

    getStore()->length = 0;

    return JNI_VERSION_1_6;
}

extern "C"
JNIEXPORT jint JNICALL
Java_com_cloudmade_ndksample_store_Store_getCount(JNIEnv *env, jobject thiz) {
    return getStore()->length;
}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_cloudmade_ndksample_store_Store_getString(JNIEnv *env, jobject thiz, jstring key) {
    StoreEntry *entry = findEntry(env, getStore(), key);
    if (isEntryValid(env, entry, STORETYPE_STRING)) {
        return env->NewStringUTF(entry->value.string);
    } else {
        return NULL;
    }
}

extern "C"
JNIEXPORT void JNICALL
Java_com_cloudmade_ndksample_store_Store_setString(JNIEnv *env, jobject thiz, jstring key,
                                                   jstring value) {
    StoreEntry *entry = allocateEntry(env, getStore(), key);
    if (entry != NULL) {
        entry->type = STORETYPE_STRING;
        jsize stringLength = env->GetStringUTFLength(value);
        entry->value.string = new char[stringLength + 1];
        env->GetStringUTFRegion(value, 0, stringLength, entry->value.string);
        entry->value.string[stringLength] = '\0';

        env->CallVoidMethod(thiz, onSuccessStringMethod,
                            (jstring) env->NewStringUTF(entry->value.string));
    }
}

extern "C"
JNIEXPORT jint JNICALL
Java_com_cloudmade_ndksample_store_Store_getInteger(JNIEnv *env, jobject thiz, jstring key) {
    StoreEntry *entry = findEntry(env, getStore(), key);
    if (isEntryValid(env, entry, STORETYPE_INTEGER)) {
        return entry->value.integer;
    } else {
        return 0;
    }
}

extern "C"
JNIEXPORT void JNICALL
Java_com_cloudmade_ndksample_store_Store_setInteger(JNIEnv *env, jobject thiz, jstring key,
                                                    jint value) {
    StoreEntry *entry = allocateEntry(env, getStore(), key);
    if (entry != NULL) {
        entry->type = STORETYPE_INTEGER;
        entry->value.integer = value;

        env->CallVoidMethod(thiz, onSuccessIntMethod, (jint) entry->value.integer);
    }
}

extern "C"
JNIEXPORT jobject JNICALL
Java_com_cloudmade_ndksample_store_Store_getColor(JNIEnv *env, jobject thiz, jstring key) {
    StoreEntry *entry = findEntry(env, getStore(), key);
    if (isEntryValid(env, entry, STORETYPE_COLOR)) {
        return entry->value.color;
    } else {
        return NULL;
    }
}

extern "C"
JNIEXPORT void JNICALL
Java_com_cloudmade_ndksample_store_Store_setColor(JNIEnv *env, jobject thiz, jstring key,
                                                  jobject value) {
    StoreEntry *entry = allocateEntry(env, getStore(), key);
    if (entry != NULL) {
        entry->type = STORETYPE_COLOR;
        entry->value.color = env->NewGlobalRef(value);

        env->CallVoidMethod(thiz, onSuccessColorMethod, entry->value.color);
    }
}

extern "C"
JNIEXPORT jobjectArray JNICALL
Java_com_cloudmade_ndksample_store_Store_getStringArray(JNIEnv *env, jobject thiz, jstring key) {
    StoreEntry *entry = findEntry(env, getStore(), key);
    if (isEntryValid(env, entry, STORETYPE_STRING_ARRAY)) {
        jobjectArray javaArray = env->NewObjectArray(entry->length, stringClass, NULL);
        for (int32_t i = 0; i < entry->length; i++) {
            jstring string = env->NewStringUTF(entry->value.stringArray[i]);
            env->SetObjectArrayElement(javaArray, i, string);
            env->DeleteLocalRef(string);
        }
        return javaArray;
    } else {
        return NULL;
    }
}

extern "C"
JNIEXPORT void JNICALL
Java_com_cloudmade_ndksample_store_Store_setStringArray(JNIEnv *env, jobject thiz, jstring key,
                                                        jobjectArray value) {
    StoreEntry *entry = allocateEntry(env, getStore(), key);
    if (entry != NULL) {
        jsize length = env->GetArrayLength(value);
        char **array = new char *[length];
        for (int32_t i = 0; i < length; i++) {
            jstring string = static_cast<jstring>(env->GetObjectArrayElement(value, i));
            jsize stringLength = env->GetStringUTFLength(string);
            array[i] = new char[stringLength + 1];
            env->GetStringUTFRegion(string, 0, stringLength, array[i]);
            array[i][stringLength] = '\0';
            env->DeleteLocalRef(string);
        }
        entry->type = STORETYPE_STRING_ARRAY;
        entry->length = length;
        entry->value.stringArray = array;
    }
}

extern "C"
JNIEXPORT jintArray JNICALL
Java_com_cloudmade_ndksample_store_Store_getIntegerArray(JNIEnv *env, jobject thiz, jstring key) {
    StoreEntry *entry = findEntry(env, getStore(), key);
    if (isEntryValid(env, entry, STORETYPE_INTEGER_ARRAY)) {
        jintArray javaArray = env->NewIntArray(entry->length);
        env->SetIntArrayRegion(javaArray, 0, entry->length, entry->value.integerArray);
        return javaArray;
    } else {
        return NULL;
    }
}

extern "C"
JNIEXPORT void JNICALL
Java_com_cloudmade_ndksample_store_Store_setIntegerArray(JNIEnv *env, jobject thiz, jstring key,
                                                         jintArray value) {
    StoreEntry *entry = allocateEntry(env, getStore(), key);
    if (entry != NULL) {

        jsize length = env->GetArrayLength(value);
        int32_t *array = new int32_t[length];
        env->GetIntArrayRegion(value, 0, length, array);
        entry->type = STORETYPE_INTEGER_ARRAY;
        entry->length = length;
        entry->value.integerArray = array;
    }
}

extern "C"
JNIEXPORT jobjectArray JNICALL
Java_com_cloudmade_ndksample_store_Store_getColorArray(JNIEnv *env, jobject thiz, jstring key) {
    StoreEntry *entry = findEntry(env, getStore(), key);
    if (isEntryValid(env, entry, STORETYPE_COLOR_ARRAY)) {
        jobjectArray javaArray = env->NewObjectArray(entry->length, colorClass, NULL);
        for (int32_t i = 0; i < entry->length; i++) {
            env->SetObjectArrayElement(javaArray, i, entry->value.colorArray[i]);
        }
        return javaArray;
    } else {
        return NULL;
    }
}

extern "C"
JNIEXPORT void JNICALL
Java_com_cloudmade_ndksample_store_Store_setColorArray(JNIEnv *env, jobject thiz, jstring key,
                                                       jobjectArray value) {
    StoreEntry *entry = allocateEntry(env, getStore(), key);
    if (entry != NULL) {
        jsize length = env->GetArrayLength(value);
        jobject *array = new jobject[length];
        for (int32_t i = 0; i < length; i++) {
            jobject localColor = env->GetObjectArrayElement(value, i);
            array[i] = env->NewGlobalRef(localColor);
            env->DeleteLocalRef(localColor);
        }
        entry->type = STORETYPE_COLOR_ARRAY;
        entry->length = length;
        entry->value.colorArray = array;
    }
}