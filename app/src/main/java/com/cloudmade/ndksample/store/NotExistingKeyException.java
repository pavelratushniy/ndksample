package com.cloudmade.ndksample.store;

public class NotExistingKeyException extends Exception {

    public NotExistingKeyException(String message) {
        super(message);
    }
}
