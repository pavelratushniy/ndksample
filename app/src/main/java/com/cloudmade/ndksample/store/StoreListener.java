package com.cloudmade.ndksample.store;

public interface StoreListener {

    void onSuccess(int value);

    void onSuccess(String value);

    void onSuccess(Color value);
}
