package com.cloudmade.ndksample.store;

public class StoreFullException extends RuntimeException {

    public StoreFullException(String message) {
        super(message);
    }
}
