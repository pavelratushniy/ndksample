package com.cloudmade.ndksample.store;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.cloudmade.ndksample.databinding.FragmentStoreBinding;

import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;

public class StoreFragment extends Fragment {

    private Store store = new Store(new StoreListener() {
        @Override
        public void onSuccess(int value) {
            displayMessage(String.format(Locale.US, "Integer '%1$d' successfuly saved", value));
        }

        @Override
        public void onSuccess(String value) {
            displayMessage(String.format(Locale.US, "String '%1$s' successfuly saved", value));
        }

        @Override
        public void onSuccess(Color value) {
            displayMessage(String.format(Locale.US, "Color '%1$s' successfuly saved", value));
        }
    });

    private FragmentStoreBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentStoreBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ArrayAdapter<StoreType> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, StoreType.values());

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        binding.sType.setAdapter(adapter);

        binding.btnGetValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onGetValue();
            }
        });

        binding.btnSetValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSetValue();
            }
        });
    }

    private void onGetValue() {
        String key = binding.etKey.getText().toString();
        StoreType type = (StoreType) binding.sType.getSelectedItem();

        try {
            switch (type) {
                case STRING:
                    binding.etValue.setText(store.getString(key));
                    break;
                case INTEGER:
                    binding.etValue.setText(String.format(Locale.US, "%1d", store.getInteger(key)));
                    break;
                case COLOR:
                    binding.etValue.setText(store.getColor(key).toString());
                    break;
                case STRING_ARRAY:
                    binding.etValue.setText(Arrays.toString(store.getStringArray(key)));
                    break;
                case INTEGER_ARRAY:
                    binding.etValue.setText(Arrays.toString(store.getIntegerArray(key)));
                    break;
                case COLOR_ARRAY:
                    binding.etValue.setText(Arrays.toString(store.getColorArray(key)));
                    break;
            }
        } catch (InvalidTypeException | NotExistingKeyException e) {
            displayMessage(e.getMessage());
        }
    }

    private void onSetValue() {
        String key = binding.etKey.getText().toString();
        String value = binding.etValue.getText().toString();
        StoreType type = (StoreType) binding.sType.getSelectedItem();

        try {
            switch (type) {
                case STRING:
                    store.setString(key, value);
                    break;
                case INTEGER:
                    store.setInteger(key, Integer.parseInt(value));
                    break;
                case COLOR:
                    store.setColor(key, new Color(value));
                    break;
                case STRING_ARRAY:
                    store.setStringArray(key, value.split(","));
                    break;
                case INTEGER_ARRAY:
                    String[] split = value.split(",");
                    int[] intArr = new int[split.length];
                    for (int i = 0; i < split.length; i++) {
                        intArr[i] = Integer.parseInt(split[i]);
                    }
                    store.setIntegerArray(key, intArr);
                    break;
                case COLOR_ARRAY:
                    split = value.split(",");
                    Color[] colorArr = new Color[split.length];
                    for (int i = 0; i < split.length; i++) {
                        colorArr[i] = new Color(split[i]);
                    }
                    store.setColorArray(key, colorArr);
                    break;
            }
        } catch (StoreFullException e) {
            displayMessage(e.getMessage());
        } catch (Exception e) {
            displayMessage(String.format("Incorrect value %s", e.getMessage()));
        }
    }

    private void displayMessage(String message) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show();
    }
}
