package com.cloudmade.ndksample.store;

public enum StoreType {
    STRING,
    INTEGER,
    COLOR,
    STRING_ARRAY,
    INTEGER_ARRAY,
    COLOR_ARRAY
}
