package com.cloudmade.ndksample.store;

public class Store implements StoreListener {

    private StoreListener listener;

    Store(StoreListener listener) {
        this.listener = listener;
    }

    public native int getCount();

    public native String getString(String key) throws InvalidTypeException, NotExistingKeyException;

    public native void setString(String key, String value);

    public native int getInteger(String key) throws InvalidTypeException, NotExistingKeyException;

    public native void setInteger(String key, int value);

    public native Color getColor(String key) throws InvalidTypeException, NotExistingKeyException;

    public native void setColor(String key, Color value);

    public native String[] getStringArray(String key) throws InvalidTypeException, NotExistingKeyException;

    public native void setStringArray(String key, String[] value);

    public native int[] getIntegerArray(String key) throws InvalidTypeException, NotExistingKeyException;

    public native void setIntegerArray(String key, int[] value);

    public native Color[] getColorArray(String key) throws InvalidTypeException, NotExistingKeyException;

    public native void setColorArray(String key, Color[] value);

    @Override
    public void onSuccess(int value) {
        listener.onSuccess(value);
    }

    @Override
    public void onSuccess(String value) {
        listener.onSuccess(value);
    }

    @Override
    public void onSuccess(Color value) {
        listener.onSuccess(value);
    }
}
