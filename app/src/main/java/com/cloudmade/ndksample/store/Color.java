package com.cloudmade.ndksample.store;

import androidx.annotation.NonNull;

public class Color {

    private int color;

    public Color(String color) {
        this.color = android.graphics.Color.parseColor(color);
    }

    @NonNull
    @Override
    public String toString() {
        return String.format("#%06X", color);
    }
}
